<?php
/**
 * @file
 * Ranks system for drupal 6.x Install file
 */
function ranks_schema() {
  $schema['ranks'] = array(
    'description' => t('Table to hold the ranks.'),
    'fields' => array(
      'rid' => array(
        'description' => t('The primary identifier for a rank.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'name' => array(
        'description' => t('The name of the rank.'),
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
        'default' => ''),
      'shortname' => array(
        'description' => t('The abbreviated name of the rank.'),
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
        'default' => ''),
      'rankimage' => array(
        'description' => t('Name of the file for the picture/insignia of the rank.'),
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => ''),
      'sorting' => array(
        'description' => t('The sort order of the ranks.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'mintime' => array(
        'description' => t('The minimum amount of time for a rank.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE),
      'maxnumber' => array(
        'description' => t('The maximum number in a rank.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE),
      'roles' => array(
        'description' => t('the roles this rank is in.'),
        'type' => 'text',
        'not null' => FALSE)
      ),
    'primary key' => array('rid'),
  );
  $schema['ranks_roster'] = array(
    'description' => t('Table to hold the roster.'),
    'fields' => array(
      'roster_id' => array(
        'description' => t('The primary identifier for a rank.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'uid' => array(
        'description' => t('The user id for the rank.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      'rank_id' => array(
        'description' => t('The ID of the rank the uid holds'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0),
      'date' => array(
        'description' => t('Date the uid got rank.'),
        'type' => 'datetime',
        'not null' => TRUE),
      'current' => array(
        'description' => t('Designates if this is users current rank'),
        'type' => 'int',
        'size' =>  'tiny',
        'not null' => TRUE,
        'default' => 0),
      'afk' => array(
        'description' => t('Designates if user is afk'),
        'type' => 'int',
        'size' =>  'tiny',
        'not null' => TRUE,
        'default' => 0),
      'notes' => array(
        'description' => t('Notes for user at current rank'),
        'type' => 'text',
        'not null' => FALSE),
      'sorting' => array(
        'description' => t('Sort order for users at current rank'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0),
      ),
    'primary key' => array('roster_id'),
  );
  return $schema;
}
function ranks_install() {
  // Create my tables.
  drupal_install_schema('ranks');
  db_query("INSERT INTO {ranks} (rid, name, shortname, rankimage, sorting, mintime, maxnumber) VALUES (1, 'Medal of Honor', 'MOH', 'moh.gif', 0, 4, 4),
(2, 'Command in Chief', 'CIC', 'cic.gif', 1, 4, 4),
(3, 'General of the Army', 'GOA', 'goa.gif', 2, 4, 4),
(4, 'General', 'GEN', 'gen.gif', 3, 4, 4),
(5, 'Lieutenant General', 'Lt.GEN', 'lt_gen.gif', 4, 4, 4),
(6, 'Major General', 'M.GEN', 'm_gen.gif', 5, 4, 4),
(7, 'Brigadier General', 'B.GEN', 'b_gen.gif', 6, 4, 4),
(8, 'Colonel', 'COL', 'col.gif', 7, 4, 4),
(9, 'Lieutenant Colonel', 'Lt.COL', 'lt_col.gif', 8, 4, 4),
(10, 'Major', 'MAJ', 'maj.gif', 9, 4, 4),
(11, 'Captain', 'CPT', 'cpt.gif', 10, 4, 4),
(12, 'First Lieutenant', '1st.LT', '1st_lt.gif', 11, 4, 4),
(13, 'Second Lieutenant', '2nd.LT', '2nd_lt.gif', 12, 4, 4),
(14, 'Sergeant Major of Army', 'SMA', 'sma.gif', 13, 4, 4),
(15, 'Command Sergeant Major', 'CSM', 'csm.gif', 14, 4, 4),
(16, 'Sergeant Major', 'SGM', 'sgm.gif', 15, 4, 4),
(17, 'Master Sergeant', 'MSG', 'm_sgt.gif', 16, 4, 4),
(18, 'First Sergeant', '1SG', '1st_sgt.gif', 17, 4, 4),
(19, 'Sergeant First Class', 'SFC', 'sfc.gif', 18, 4, 4),
(20, 'Staff Sergeant', 'SSG', 's_sgt.gif', 19, 4, 4),
(21, 'Sergeant', 'SGT', 'sgt.gif', 20, 4, 4),
(22, 'Corporal', 'CPL', 'cpl.gif', 21, 4, 4),
(23, 'Specialist', 'SPC', 'spl.png', 22, 4, 4),
(24, 'Private First Class', 'PFC', 'pfc.gif', 23, 4, 4),
(25, 'Private', 'PVT', 'pvt.gif', 24, 4, 4),
(26, 'Recruit', 'RCT', 'recruit.png', 25, 4, 4)");
  variable_set('ranks_debug', 0);
  variable_set('ranks_default', 26);
  variable_set('ranks_afk', 0);
}
function ranks_uninstall() {
  // Drop my tables.
  drupal_uninstall_schema('ranks');
  variable_del('ranks_debug');
  variable_del('ranks_default');
  variable_del('ranks_afk');
}
function ranks_update_6200() {
  db_add_field($ret, 'ranks_roster', 'current', array(
        'description' => t('Designates if this is users current rank'),
        'type' => 'int',
        'size' =>  'tiny',
        'not null' => TRUE,
        'default' => 0));
  db_add_field($ret, 'ranks_roster', 'afk', array(
        'description' => t('Designates if user is afk'),
        'type' => 'int',
        'size' =>  'tiny',
        'not null' => TRUE,
        'default' => 0));
  db_add_field($ret, 'ranks_roster', 'notes', array(
        'description' => t('Notes for user at current rank'),
        'type' => 'text',
        'not null' => FALSE));
  db_add_field($ret, 'ranks_roster', 'sorting', array(
        'description' => t('Sort order for users at current rank'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0));
  variable_set('ranks_afk', 0);
  db_query("UPDATE {ranks_roster} SET current = 1");
}