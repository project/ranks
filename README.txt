
A system to control a guild/clan rank and associate each rank to a role.

Installation
------------

Follow general drupal module instructions.  Upload the entire ranks directory to your module directory on your server generally sites/all/modules.
enable the module in your admin panel.

Upgrade
-------
Be sure to run upgrade.php after installing new version.

Author
------
Mitch Tuck
Website www.matuckcomputing.com
matuck@matuckcomputing.com